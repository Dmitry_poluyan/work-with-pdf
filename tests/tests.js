'use strict';

const config = require('./config');
const toPdf = require('../src/imageToPdf');

// toPdf(config.path, config.output);

var path = [];
for(let i = 0; i < 25; i++){
    path.push(`${__dirname}/pdf/Harrison_Hapi_MEAP_V09_ch1-${i}.png`);
}
// console.log(path);
toPdf(path, config.output);