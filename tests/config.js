'use strict';

const fs  = require ('fs');

module.exports = {
    path:[
        `${__dirname}/pdf/Harrison_Hapi_MEAP_V09_ch1-0.png`,
        `${__dirname}/pdf/Harrison_Hapi_MEAP_V09_ch1-1.png`,
        `${__dirname}/pdf/Harrison_Hapi_MEAP_V09_ch1-2.png`
    ],
    output: 'output.pdf',
    pdf: './pdf/Harrison_Hapi_MEAP_V09_ch1.pdf'
};