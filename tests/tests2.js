'use strict';

const config = require('./config');
const slide = new (require('pdf-image-pack'))();

// slide.output(config.path, config.output, function(err, doc){
//     if(err){
//         console.error(err);
//     }
//     console.log('finish output');
// });

var path = [];
for(let i = 0; i < 25; i++){
    path.push(`${__dirname}/pdf/Harrison_Hapi_MEAP_V09_ch1-${i}.png`);
}

slide.output(path, config.output, function(err, doc){
    if(err){
        console.error(err);
    }
    console.log('finish output');
});