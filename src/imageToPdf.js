'use strict';

const PDFDocument = require ('pdfkit');
const fs  = require ('fs');

module.exports = (path, output)=> {
    var doc = new PDFDocument();

    doc.pipe(fs.createWriteStream(output));

    if(path && path.length && path.length > 0){
        for(let i = 0; i < path.length; i++){

            doc.image(path[i], 0, 0);

            if(i < path.length-1){
                doc.addPage();
            }
        }
    }
    doc.end();
};
